const express = require('express');
const app = express();
const port = 3000;

const request = require('request');
app.get('/', (req, res) =>{
    request('https://api.chucknorris.io/jokes/random', function (error, response, body){
        if(error){
            res.status(400).send('Error');
        }else{
            res.send(body);
        }
    });
}); //req = request, res = response
app.post('/', (req, res) => res.send('Halo Post'));
app.listen(port, () => console.log('App Starting'));